<?php
namespace Show\MydeskBundle\Controller;
use MyProject\Proxies\__CG__\OtherProject\Proxies\__CG__\stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
    * @Route("/", name="index")
    * @Template()
    */
    public function indexAction()
    {
        $data = date('d.m.Y');
        $godzina = date('h:i:s');
    return array(
        'slog' => 'Powitanie',
        'data' => $data,
        'godzina' => $godzina
    );
    }

    /**
     * @Route("/array_twig", name="array_twig")
     * @Template()
     */
    public function array_twigAction()
    {
        // Nowy obiekt
        $client = new \SoapClient('http://test.szop.pl.easypack24.net:8080/Dispatch?wsdl');
        // Odwołanie do metody
        $result = $client->getDispatchPriceNames();
        // Zmieniam obiekt na tablice
        $result = $result->return;
        // Przekazuje konkretną tablicę nazwaną dowolnie np. "soap" do widoku "soap.html.twig".
        // W pustym @Template mamy ustalone z automatu generowanie nazy metody akcji "ShowMydeskBundle:Default:soap.html.twig"
        // Możemy pominąć @Template renderując widok w następujący sposób:
        return $this->render('ShowMydeskBundle:Default:array_twig.html.twig', array('soap' => $result));
    }

    /**
     * @Route("/obj_twig", name="obj_twig")
     */
    public function obj_twigAction()
    {
        // Nowy obiekt
        $client = new \SoapClient('http://test.szop.pl.easypack24.net:8080/Dispatch?wsdl');
        // Odwołanie do metody
        $result = $client->getDispatchPriceNames();
        // Przekazanie do widoku
        return $this->render('ShowMydeskBundle:Default:obj_twig.html.twig', array('obj' => $result));
    }

    /**
     * @Route("/loop_twig", name="loop_twig")
     */
    public function loop_twigAction()
    {
        // Nowy obiekt
        $client = new \SoapClient('http://test.szop.pl.easypack24.net:8080/Dispatch?wsdl');
        // Odwołanie do metody
        $result = $client->getDispatchPriceNames();
        // Zmieniam obiekt na tablice
        $result = $result->return;
        // Przekazuje konkretną tablicę nazwaną dowolnie np. "soap" do widoku "soap.html.twig".
        // W pustym @Template mamy ustalone z automatu generowanie nazy metody akcji "ShowMydeskBundle:Default:soap.html.twig"
        // Możemy pominąć @Template renderując widok w następujący sposób:
        return $this->render('ShowMydeskBundle:Default:loop_twig.html.twig', array('loop' => $result));
    }

    /**
     * @Route("/mfi_twig", name="mfi_twig")
     */
    public function mfi_twigAction()
    {
        // Nowy obiekt
        $client = new \SoapClient('http://test.szop.pl.easypack24.net:8080/Dispatch?wsdl');
        // Odwołanie do metody
        $result = $client->getDispatchPriceNames();
        // Zmieniam obiekt na tablice
        $result = $result->return;
        // Przekazuje konkretną tablicę nazwaną dowolnie np. "soap" do widoku "soap.html.twig".
        // W pustym @Template mamy ustalone z automatu generowanie nazy metody akcji "ShowMydeskBundle:Default:soap.html.twig"
        // Możemy pominąć @Template renderując widok w następujący sposób:
        return $this->render('ShowMydeskBundle:Default:mfi_twig.html.twig', array('mfi' => $result));
    }

    /**
     * @Route("/block_twig", name="block_twig")
     */
    public function block_twigAction()
    {
        // Nowy obiekt
        $client = new \SoapClient('http://test.szop.pl.easypack24.net:8080/Dispatch?wsdl');
        // Odwołanie do metody
        $result = $client->getDispatchPriceNames();
        // Zmieniam obiekt na tablice
        $result = $result->return;
        // Przekazuje konkretną tablicę nazwaną dowolnie np. "soap" do widoku "soap.html.twig".
        // W pustym @Template mamy ustalone z automatu generowanie nazy metody akcji "ShowMydeskBundle:Default:soap.html.twig"
        // Możemy pominąć @Template renderując widok w następujący sposób:
        return $this->render('ShowMydeskBundle:Default:block_twig.html.twig', array('block' => $result));
    }

    /**
     * @Route("/use_twig", name="use_twig")
     * @Template()
     */
    public function use_twigAction()
    {
        $var = "test";
        return array('var' => $var);
    }

    /**
     * @Route("/useit_twig", name="useit_twig")
     * @Template()
     */
    public function useit_twigAction()
    {
        $var = "test";
        return array('var' => $var);
    }

    /**
     * @Route("/filter_twig", name="filter_twig")
     * @Template()
     */
    public function filter_twigAction()
    {
        $var = "ŻÓŁW";
        $published_at = "";
        $zm = "/%$#";
        return array(
            'var' => $var,
            'published_at' => $published_at,
            'zm' => $zm
        );
    }

    /**
     * @Route("/function_twig", name="function_twig")
     * @Template()
     */
    public function function_twigAction()
    {
        $var = "ŻÓŁW";
        return array(
            'var' => $var
        );
    }

    /**
     * @Route("/db_twig", name="db_twig")
     * @Template()
     */
    public function db_twigAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('ShowMydeskBundle:Name')->findAll();
        return array(
            'entities' => $entities
        );
    }
}